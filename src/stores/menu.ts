import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Sweet from "@/types/Menu";
import type Drink from "@/types/Menu";
import type Food from "@/types/Menu";

export const useMenuStore = defineStore("product", () => {
  const orderList = ref<
    { id: number; name: string; price: number; qty: number }[]
  >([]);
  const drinks = ref<Drink[]>([
    {
      id: 1,
      name: "คาปูชิโน่",
      price: 80,
      img: "./cappuccino.jpg",
      qty: 1,
    },
    { id: 2, name: "มอคค่า", price: 80, img: "./mocha.jpg", qty: 1 },
    { id: 3, name: "ลาเต้เย็น", price: 80, img: "./latte.jpg", qty: 1 },
    { id: 4, name: "เอสเปรสโซ่", price: 80, img: "./es.jpeg", qty: 1 },
    {
      id: 5,
      name: "ช็อคโก้เย็น",
      price: 80,
      img: "https://bakingmischief.com/wp-content/uploads/2019/05/iced-mocha-image-square.jpg",
      qty: 1,
    },
    {
      id: 6,
      name: "อเมริกาโน่",
      price: 80,
      img: "./americano.jpg",
      qty: 1,
    },
    { id: 7, name: "โกโก้เย็น", price: 80, img: "./choco.jpg", qty: 1 },
    {
      id: 8,
      name: "ชามะนาว",
      price: 70,
      img: "./chamanawww.jpg",
      qty: 1,
    },
    {
      id: 9,
      name: "บราวน์ชูก้า",
      price: 85,
      img: "https://www.foxyfolksy.com/wp-content/uploads/2020/06/tiger-sugar-milk-drink.jpg",
      qty: 1,
    },
    {
      id: 10,
      name: "ช็อคโกร้อน",
      price: 70,
      img: "https://onedishkitchen.com/wp-content/uploads/2017/12/hot-chocolate-one-dish-kitchen-square-1200-250x250.jpg",
      qty: 1,
    },
    { id: 11, name: "ชาไทย", price: 70, img: "./chathai.png", qty: 1 },
    {
      id: 12,
      name: "ชาเขียว",
      price: 80,
      img: "./greentea.jpg",
      qty: 1,
    },
    { id: 13, name: "สมูทตี้", price: 90, img: "./smooth.jpg", qty: 1 },
    {
      id: 14,
      name: "เฟรชโคโค่นัท",
      price: 75,
      img: "./cappuccino.jpg",
      qty: 1,
    },
    {
      id: 15,
      name: "นมสดเย็น",
      price: 70,
      img: "https://www.thehealthymaven.com/wp-content/uploads/2017/05/Iced-Golden-Milk-Latte-FI.jpg",
      qty: 1,
    },
    {
      id: 16,
      name: "สตอเบอรี่ชีสเค้กปั่น",
      price: 70,
      img: "./staw.jpg",
      qty: 1,
    },
  ]);

  const sweets = ref<Sweet[]>([
    { id: 17, name: "ช็อคโก้เค้ก", price: 45, img: "./222.jpg", qty: 1 },
    { id: 18, name: "ครัวซอง", price: 30, img: "./333.jpg", qty: 1 },
    { id: 19, name: "บานอฟฟี่", price: 55, img: "./444.jpg", qty: 1 },
    { id: 20, name: "แพนเค้ก", price: 75, img: "./pancake.jpg", qty: 1 },
    {
      id: 21,
      name: "ครอฟเฟิล",
      price: 30,
      img: "./crof.jpg",
      qty: 1,
    },
    { id: 22, name: "ทาร์ตไข่", price: 35, img: "./tart.jpg", qty: 1 },
    { id: 23, name: "วาฟเฟิล", price: 75, img: "./666.jpg", qty: 1 },
    {
      id: 24,
      name: "ช็อคโก้มาก้า",
      price: 80,
      img: "./777.jpg",
      qty: 1,
    },
    {
      id: 25,
      name: "ฮันนี่โทสต์",
      price: 80,
      img: "./ht.jpg",
      qty: 1,
    },
    {
      id: 26,
      name: "ช็อคโกลาวา",
      price: 45,
      img: "./lava.jpeg",
      qty: 1,
    },
    { id: 27, name: "บราวน์นี่", price: 45, img: "./888.jpg", qty: 1 },
    { id: 28, name: "คุกกี้", price: 35, img: "./999.jpg", qty: 1 },
    { id: 29, name: "พุดดิ้ง", price: 70, img: "./000.jpg", qty: 1 },
    {
      id: 30,
      name: "ชีสเค้ก",
      price: 45,
      img: "./ck.jpg",
      qty: 1,
    },
    {
      id: 31,
      name: "เครปเค้ก",
      price: 40,
      img: "https://www.sidechef.com/article/ad8926ad-812d-4cac-b249-6cba45dc3dad.jpg?d=1408x1120",
      qty: 1,
    },
    {
      id: 32,
      name: "บิงซู",
      price: 80,
      img: "./bingsu.jpg",
      qty: 1,
    },
  ]);

  const foods = ref<Food[]>([
    {
      id: 33,
      name: "สปาเก็ตตี้",
      price: 150,
      img: "./สปกตe.jpg",
      qty: 1,
    },
    {
      id: 34,
      name: "คาโบน่าร่า",
      price: 180,
      img: "./คบนรe.jpg",
      qty: 1,
    },
    { id: 35, name: "ข้าวผัด", price: 89, img: "./หหหe.jpg", qty: 1 },
    { id: 36, name: "สตูว์", price: 159, img: "./สตนe.jpg", qty: 1 },
    {
      id: 37,
      name: "มันบด",
      price: 79,
      img: "./มบe.jpg",
      qty: 1,
    },
    {
      id: 38,
      name: "พิซซ่า",
      price: 189,
      img: "./พิซส่าe.jpg",
      qty: 1,
    },
    { id: 39, name: "ผักโขม", price: 120, img: "./ผขขขe.jpg", qty: 1 },
    {
      id: 40,
      name: "มัคแอนด์ชีส",
      price: 119,
      img: "./มคe.jpg",
      qty: 1,
    },
    {
      id: 41,
      name: "แซนวิช",
      price: 80,
      img: "./swwwwe.jpg",
      qty: 1,
    },
    {
      id: 42,
      name: "สเต็กเนื้อ",
      price: 220,
      img: "./สนตe.jpg",
      qty: 1,
    },
    { id: 43, name: "สลัดกุ้ง", price: 159, img: "./สดe.jpg", qty: 1 },
    { id: 44, name: "เบอร์เกอร์", price: 99, img: "./บกe.jpg", qty: 1 },
    {
      id: 45,
      name: "เบเนดิกต์",
      price: 120,
      img: "./ขบนดe.jpg",
      qty: 1,
    },
    {
      id: 46,
      name: "ไข่เจียว",
      price: 99,
      img: "./อลe.jpg",
      qty: 1,
    },
    {
      id: 47,
      name: "สเต็กปลา",
      price: 129,
      img: "./สตปe.jpg",
      qty: 1,
    },
    {
      id: 48,
      name: "พอร์คชอป",
      price: 179,
      img: "./พคe.jpg",
      qty: 1,
    },
  ]);

  const dialog = ref(false);
  const dialog2 = ref(false);
  const cash = ref(0);
  const discount = ref(0);

  const add = (item: {
    id: number;
    name: string;
    price: number;
    qty: number;
  }) => {
    const items = orderList.value.find((_item) => _item.id === item.id);
    const index = orderList.value.findIndex((_item) => _item.id === item.id);

    if (items) {
      item.qty += 1;
      orderList.value.splice(index, 1, item);
    } else {
      orderList.value.push(item);
    }
  };

  const deleteorderlist = (item: { id: number; qty: number }): void => {
    const index = orderList.value.findIndex((_item) => _item.id === item.id);
    orderList.value.splice(index, 1);
    item.qty = 1;
  };

  const deleteAllorderlist = () => {
    const all = orderList.value.length;
    orderList.value.splice(0, all);
  };

  const inc = (item: {
    id: number;
    name: string;
    price: number;
    qty: number;
  }) => {
    const index = orderList.value.findIndex((_item) => _item.id === item.id);
    item.qty += 1;
    orderList.value.splice(index, 1, item);
  };

  const dec = (item: {
    id: number;
    name: string;
    price: number;
    qty: number;
  }) => {
    const index = orderList.value.findIndex((_item) => _item.id === item.id);
    if (item.qty < 2) {
      return;
    } else {
      item.qty -= 1;
      orderList.value.splice(index, 1, item);
    }
  };

  const totalPrice = computed(function () {
    return orderList.value.reduce(
      (sum, item) => sum + item.price * item.qty,
      0
    );
  });
  const change = computed(function () {
    if (cash.value == 0) {
      return;
    } else {
      return orderList.value.reduce(() => cash.value - totalPrice.value, 0);
    }
  });

  const savecash = (money: number) => {
    cash.value = money;
    dialog.value = false;
  };

  const discountest = computed(function () {
    if (discount.value == 0) {
      return;
    } else {
      return orderList.value.reduce(() => totalPrice.value - discount.value, 0);
    }
  });

  return {
    drinks,
    sweets,
    foods,
    add,
    orderList,
    deleteorderlist,
    inc,
    dec,
    totalPrice,
    deleteAllorderlist,
    dialog,
    savecash,
    cash,
    change,
    discountest,
    dialog2,
  };
});
