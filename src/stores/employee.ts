import { ref } from "vue";
import { defineStore } from "pinia";
import employeesService from "@/services/employee";
import type Employees from "@/types/Empolyees";
import { useMessageStore } from "./message";

export const useEmployeeStore = defineStore("employee", () => {
  const dialog = ref(false);
  const dialog1 = ref(false);
  const isTable = ref(true);
  const messageStore = useMessageStore();
  const editedEmployee = ref<Employees & { files: File[] }>({
    id: 0,
    name: "",
    house_no: "",
    village_no: "",
    sub_district: "",
    district: "",
    province: "",
    email: "",
    tel: "",
    position: "",
    hourly_wage: 0,
    userId: 0,
    image: "No_Image_Available.jpg",
    files: [],
  });
  // const lastId = 6;
  const employees = ref<Employees[]>([]);

  async function getEmployees() {
    try {
      const res = await employeesService.getEmployees({});
      employees.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Employee ได้");
    }
  }

  async function getViewEmployees() {
    try {
      const res = await employeesService.getViewEmployees({});
      employees.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Employee ได้");
    }
  }

  async function deleteEmployee(id: number) {
    try {
      const res = await employeesService.deleteEmployee(id);
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Employee ได้");
    }
  }

  async function saveEmployee() {
    // console.log(editedEmployee);
    try {
      // Get the current list of employees
      // const res = await employeesService.getEmployees();
      // const currentEmployees = res.data;
      // Save or update the employee depending on whether it has an id or not
      if (editedEmployee.value.id) {
        const res = await employeesService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        const res = await employeesService.saveEmployee(editedEmployee.value);
      }

      dialog.value = false;
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Employee ได้");
    }
  }

  // async function saveEmployee() {
  //   try {
  //     if (editedEmployee.value.id) {
  //       const res = await employeesService.updateEmployee(
  //         editedEmployee.value.id,
  //         editedEmployee.value
  //       );
  //     } else {
  //       const res = await employeesService.saveEmployee(editedEmployee.value);
  //     }

  //     dialog.value = false;
  //     clearEmployee();
  //     await getEmployees();
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  const editEmployee = (employee: Employees) => {
    editedEmployee.value = JSON.parse(JSON.stringify(employee));
    dialog.value = true;
  };

  function clearEmployee() {
    editedEmployee.value = {
      id: 0,
      name: "",
      house_no: "",
      village_no: "",
      sub_district: "",
      district: "",
      province: "",
      email: "",
      tel: "",
      position: "",
      hourly_wage: 0,
      userId: 0,
      image: "No_Image_Available.jpg",
      files: [],
    };
  }

  return {
    employees,
    deleteEmployee,
    dialog,
    editedEmployee,
    clearEmployee,
    saveEmployee,
    editEmployee,
    isTable,
    dialog1,
    getEmployees,
    getViewEmployees,
  };
});
