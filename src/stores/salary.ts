import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Salary from "@/types/Salary";
import salaryService from "@/services/salary";

export const useSalary = defineStore("time", () => {
  const dialog = ref(false);
  const salary = ref<Salary[]>([]);
  const editedSalary = ref<Salary>({
    datess: 0,
    workhour: 0,
    salary: 0,
  });

  async function getSalarys() {
    try {
      const res = await salaryService.getSalarys();
      salary.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function deleteSalary(id: number) {
    try {
      const res = await salaryService.deleteSalary(id);
      await getSalarys();
    } catch (e) {
      console.log(e);
    }
  }

  async function saveSalary() {
    try {
      if (editedSalary.value.id) {
        const res = await salaryService.updateSalary(
          editedSalary.value.id,
          editedSalary.value
        );
      } else {
        const res = await salaryService.saveSalary(editedSalary.value);
      }

      dialog.value = false;
      await getSalarys();
    } catch (e) {
      console.log(e);
    }
  }

  const editSalary = (salary: Salary) => {
    editedSalary.value = { ...salary };
    dialog.value = true;
  };

  function clearTime() {
    editedSalary.value = {
      datess: 0,
      workhour: 0,
      salary: 0,
    };
  }
  return {
    editSalary,
    saveSalary,
    deleteSalary,
    getSalarys,
    dialog,
    clearTime,
  };
});
