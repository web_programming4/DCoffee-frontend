import { ref } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/services/users";

export const useUserStore = defineStore("user", () => {
  const dialog = ref(false);
  const dialog1 = ref(false);
  const isTable = ref(true);
  const editedUsers = ref<User>({ name: "", login: "", password: "" });
  // const lastId = 4;
  const users = ref<User[]>([]);
  async function getUsers() {
    try {
      const res = await userService.getUsers();
      users.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function deleteUser(id: number) {
    try {
      const res = await userService.deleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
    }
  }

  async function saveUser() {
    try {
      if (editedUsers.value.id) {
        const res = await userService.updateUser(
          editedUsers.value.id,
          editedUsers.value
        );
      } else {
        const res = await userService.saveUser(editedUsers.value);
      }

      dialog.value = false;
      clearUser();
      await getUsers();
    } catch (e) {
      console.log(e);
    }
  }

  const editUser = (user: User) => {
    editedUsers.value = { ...user };
    dialog.value = true;
  };

  function clearUser() {
    editedUsers.value = { name: "", login: "", password: "" };
  }

  const login = (loginName: string, password: string): boolean => {
    const index = users.value.findIndex((item) => item.login === loginName);
    if (index >= 0) {
      const user = users.value[index];
      if (user.password === password) {
        return true;
      }
      return false;
    }
    return false;
  };

  return {
    users,
    deleteUser,
    dialog,
    editedUsers,
    clearUser,
    saveUser,
    editUser,
    isTable,
    dialog1,
    getUsers,
  };
});
