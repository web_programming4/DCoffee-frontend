import { ref } from "vue";
import { defineStore } from "pinia";
import type Stock from "@/types/Stock";
import stockService from "@/services/stock";

export const useStockStore = defineStore("stock", () => {
  const dialog = ref(false);
  const dialog1 = ref(false);
  const editedStocks = ref<Stock>({
    name: "",
    min_qty: 0,
    qty: 0,
    unit: "",
    price_per_unit: 0,
  });
  const stocks = ref<Stock[]>([]);

  async function getStocks() {
    try {
      const res = await stockService.getStocks();
      stocks.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function deleteStock(id: number) {
    try {
      const res = await stockService.deleteStock(id);
      await getStocks();
    } catch (e) {
      console.log(e);
    }
  }

  async function saveStock() {
    try {
      if (editedStocks.value.id) {
        const res = await stockService.updateStock(
          editedStocks.value.id,
          editedStocks.value
        );
        console.log(res);
      } else {
        const res = await stockService.saveStock(editedStocks.value);
        console.log(res);
      }
      dialog.value = false;
      clear();
      await getStocks();
    } catch (e) {
      console.log(e);
    }
  }

  const editStock = (stock: Stock) => {
    editedStocks.value = { ...stock };
    dialog.value = true;
  };

  function clear() {
    editedStocks.value = {
      name: "",
      min_qty: 0,
      qty: 0,
      unit: "",
      price_per_unit: 0,
    };
  }

  return {
    stocks,
    deleteStock,
    dialog,
    editedStocks,
    clear,
    saveStock,
    editStock,
    getStocks,
    dialog1,
  };
});
