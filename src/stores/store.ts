import { ref } from "vue";
import { defineStore } from "pinia";
import type Store from "@/types/Store";
import storeService from "@/services/store";

export const useStore = defineStore("store", () => {
  const dialog = ref(false);
  const editedStores = ref<Store>({
    name: "",
    address: "",
    tel: "",
  });
  const stores = ref<Store[]>([]);

  async function getStores() {
    try {
      const res = await storeService.getStores();
      stores.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function deleteStore(id: number) {
    try {
      const res = await storeService.deleteStore(id);
      await getStores();
    } catch (e) {
      console.log(e);
    }
  }

  async function saveStore() {
    try {
      if (editedStores.value.id) {
        const res = await storeService.updateStore(
          editedStores.value.id,
          editedStores.value
        );
      } else {
        const res = await storeService.saveStore(editedStores.value);
      }

      dialog.value = false;
      clear();
      await getStores();
    } catch (e) {
      console.log(e);
    }
  }

  const editStore = (store: Store) => {
    editedStores.value = { ...store };
    dialog.value = true;
  };

  function clear() {
    editedStores.value = {
      name: "",
      address: "",
      tel: "",
    };
  }

  return {
    stores,
    deleteStore,
    dialog,
    editedStores,
    clear,
    saveStore,
    editStore,
    getStores,
  };
});
