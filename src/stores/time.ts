import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Time from "@/types/Time";
import timeService from "@/services/times";

export const useTime = defineStore("time", () => {
  const dialog = ref(false);
  const isTable = ref(true);
  const times = ref<Time[]>([]);
  const editedTimes = ref<Time>({
    cio_date: "",
    cio_time_in: "",
    cio_time_out: "",
    cio_total_hour: 0,
    employeeId: 0,
    salaryId: 0,
  });

  // watch(dialog, (newDialog, oldDialog) => {
  //   if (!newDialog) {
  //     editedTimes.value = {
  //       cio_date: "",
  //       cio_time_in: "",
  //       cio_time_out: "",
  //       cio_total_hour: 0,
  //       employeeId: 0,
  //       salaryId: 0,
  //     };
  //   }
  // });

  async function getTimes() {
    try {
      const res = await timeService.getTimes();
      times.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function deleteTime(id: number) {
    try {
      const res = await timeService.deleteTime(id);
      await getTimes();
    } catch (e) {
      console.log(e);
    }
  }

  async function saveTime() {
    try {
      if (editedTimes.value.cio_id) {
        const res = await timeService.updateTime(
          editedTimes.value.cio_id,
          editedTimes.value
        );
      } else {
        const res = await timeService.saveTime(editedTimes.value);
      }

      dialog.value = false;
      await getTimes();
      clearTime();
    } catch (e) {
      console.log(e);
    }
  }

  const editTime = (times: Time) => {
    editedTimes.value = { ...times };
    dialog.value = true;
  };

  function clearTime() {
    editedTimes.value = {
      cio_date: "",
      cio_time_in: "",
      cio_time_out: "",
      cio_total_hour: 0,
      employeeId: 0,
      salaryId: 0,
    };
  }
  return {
    times,
    getTimes,
    deleteTime,
    editTime,
    saveTime,
    dialog,
    clearTime,
    isTable,
  };
});
