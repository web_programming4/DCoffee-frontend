import { ref } from "vue";
import { defineStore } from "pinia";
import type Category from "@/types/Category";
import CategoryService from "@/services/category";
// import { useLoadingStore } from "./loading";
// import { useMessageStore } from "./message";

export const useCategoryStore = defineStore("Category", () => {
  const dialog = ref(false);
  // const dialog1 = ref(false);
  const isTable = ref(true);
  const editedCategory = ref<Category>({ name: "" });
  // const lastId = 4;
  const category = ref<Category[]>([]);
  async function getCategorys() {
    try {
      const res = await CategoryService.getCategorys();
      category.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function deleteCategory(id: number) {
    try {
      const res = await CategoryService.deleteCategory(id);
      await getCategorys();
    } catch (e) {
      console.log(e);
    }
  }

  async function saveCategory() {
    try {
      if (editedCategory.value.id) {
        const res = await CategoryService.updateCategory(
          editedCategory.value.id,
          editedCategory.value
        );
      } else {
        const res = await CategoryService.saveCategory(editedCategory.value);
      }

      dialog.value = false;
      clearCategory();
      await getCategorys();
    } catch (e) {
      console.log(e);
    }
  }

  const editCategory = (category: Category) => {
    editedCategory.value = { ...category };
    dialog.value = true;
  };

  function clearCategory() {
    editedCategory.value = { name: "" };
  }

  return {
    category,
    deleteCategory,
    dialog,
    editedCategory,
    clearCategory,
    saveCategory,
    editCategory,
    isTable,
    getCategorys,
  };
});
