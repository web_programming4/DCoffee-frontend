import { ref } from "vue";
import { defineStore } from "pinia";

export const useThemeStore = defineStore("theme", () => {
  const theme = ref("light");
  function changeTheme() {
    theme.value = theme.value === "light" ? "dark" : "light";
  }

  return { theme, changeTheme };
});
