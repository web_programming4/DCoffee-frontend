import { ref } from "vue";
import { defineStore } from "pinia";
// import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Customer from "@/types/Customer";
import customerService from "@/services/customer";
// import router from "@/router";

export const useCustomerStore = defineStore("customer", () => {
  const dialog = ref(false);
  const dialog1 = ref(false);
  const dialog2 = ref(false);
  const isTable = ref(true);
  const member = ref<Customer[]>([]);
  const nameCus = ref("");
  const telCus = ref(0);
  const pointCus = ref("");

  const messageStore = useMessageStore();
  const editedCustomer = ref<Customer>({
    name: "",
    tel: 0,
    point: "",
  });

  const customers = ref<Customer[]>([]);
  const checkcus = ref<Customer[]>([]);

  async function getCustomers() {
    try {
      const res = await customerService.getCustomers();
      customers.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function getProCustomers() {
    try {
      const res = await customerService.getViewCustomers();
      customers.value = res.data[0];
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Customer ได้");
    }
  }

  async function deleteCustomer(id: number) {
    try {
      const res = await customerService.deleteCustomer(id);
      await getProCustomers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Customer ได้");
    }
  }

  async function saveCustomer() {
    try {
      if (editedCustomer.value.id) {
        const res = await customerService.updateCustomer(
          editedCustomer.value.id,
          editedCustomer.value
        );
        member.value[0].point = editedCustomer.value.point;
      } else {
        member.value.push(editedCustomer.value);
        const res = await customerService.saveCustomer(editedCustomer.value);
      }

      dialog.value = false;
      clearCustomer();

      await getProCustomers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Customer ได้");
    }
  }

  const editCustomer = (customer: Customer) => {
    editedCustomer.value = { ...customer };
    dialog.value = true;
  };

  function clearCustomer() {
    editedCustomer.value = { name: "", tel: 0, point: "" };
  }
  function clearMember() {
    member.value = [];
  }

  function chekCus(tel: number) {
    // loadingStore.isLoading = true;
    for (let i = 0; i < checkcus.value.length; i++) {
      if (checkcus.value[i].tel === tel) {
        dialog1.value = true;
        // nameCus.value = checkcus.value[i].name;
        // telCus.value = checkcus.value[i].tel;
        // pointCus.value = checkcus.value[i].point;
        member.value.push(checkcus.value[i]);
        return;
      }
    }
    if (nameCus.value === "") {
      messageStore.showError("tel ไม่ถูกต้อง หรือ ไม่ได้เป็นสมาชิก");
    }
    console.log(dialog1);
  }

  function addcus(item: Customer) {
    checkcus.value.push(item);
    console.log(checkcus);
  }

  return {
    customers,
    deleteCustomer,
    dialog,
    editedCustomer,
    clearCustomer,
    saveCustomer,
    editCustomer,
    isTable,
    dialog1,
    getProCustomers,
    chekCus,

    getCustomers,

    nameCus,
    telCus,
    pointCus,
    dialog2,
    addcus,
    checkcus,
    member,
    clearMember,
  };
});
