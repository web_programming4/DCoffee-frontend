import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
// import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductPageStore = defineStore("productPage", () => {
  // const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const page = ref(1);
  const take = ref(2);
  const keyword = ref("");
  const products = ref<Product[]>([]);
  const lastPage = ref(0);
  watch(keyword, async (newKeyword, oldKeyword) => {
    await getProducts();
  });
  watch(page, async (newPage, oldPage) => {
    await getProducts();
  });
  watch(lastPage, async (newLastPage, oldLastPage) => {
    if (newLastPage < page.value) {
      page.value = 1;
    }
  });

  async function getProducts() {
    // loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts({
        page: page.value,
        take: take.value,
        keyword: keyword.value,
      });
      products.value = res.data.data;
      // lastPage.value = res.data.lastPage;
      console.log(products.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Product ได้");
    }
  }

  return { page, take, keyword, products, getProducts, lastPage };
});
