import type Product from "@/types/Product";
import { mdiEslint } from "@mdi/js";
import { defineStore } from "pinia";
import { ref } from "vue";

export const useChipStore = defineStore("chip", () => {
  // const sweetList = ref<{ id: number; name: string; price: number }[]>([]);
  // const sizeList = ref<{ id: number; name: string; price: number }[]>([]);
  // const typeList = ref<{ id: number; name: string; price: number }[]>([]);
  // const toppingList = ref<{ id: number; name: string; price: number }[]>([]);
  // const addOndrink = ref<{ id: number; name: string; price: number }[]>([]);

  const sweetList = ref<{ id: number; name: string }[]>([]);
  const sizeList = ref<{ id: number; name: string }[]>([]);
  const typeList = ref<{ id: number; name: string }[]>([]);
  const toppingList = ref<{ id: number; name: string }[]>([]);
  const addOndrink = ref<{ id: number; name: string }[]>([]);

  // const type = ref<{ id: number; name: string; price: number }[]>([
  //   { id: 1, name: "HOT (ร้อน)", price: 0 },
  //   { id: 2, name: "ICED (เย็น)", price: 10 },
  //   { id: 3, name: "BLENDED (ปั่น)", price: 15 },
  // ]);

  // const sweet = ref<{ id: number; name: string; price: number }[]>([
  //   { id: 4, name: "Normal 100%", price: 0 },
  //   { id: 5, name: "Less Sweet 50%", price: 0 },
  //   { id: 6, name: "Less Less Sweet 25%", price: 0 },
  //   { id: 7, name: "Not Sweet 0%", price: 0 },
  // ]);

  // const size = ref<{ id: number; name: string; price: number }[]>([
  //   { id: 8, name: "Size S (8 oz.)", price: 0 },
  //   { id: 9, name: "Size M (12 oz.)", price: 5 },
  //   { id: 10, name: "Size L (16 oz.)", price: 10 },
  // ]);

  // const topping = ref<{ id: number; name: string; price: number }[]>([
  //   { id: 1, name: "Pearl (ไข่มุก)", price: 5 },
  //   { id: 2, name: "Lava Pearl (ไข่มุกลาวา)", price: 10 },
  //   { id: 3, name: "Fruity Jelly (เยลลี่ฟรุ๊ตสลัด)", price: 10 },
  //   { id: 4, name: "Grass Jelly (เฉาก๊วย)", price: 10 },
  //   { id: 5, name: "Brown Sugar Pearl (ไข่มุกบราวน์ชูการ์)", price: 15 },
  //   { id: 6, name: "Milk Pudding (พุดดิ้งนม)", price: 15 },
  //   { id: 7, name: "Whipped Cream (วิปครีม) ", price: 20 },
  //   { id: 8, name: "Whip Cheese (วิปชีส)", price: 25 },
  // ]);

  const type = ref<{ id: number; name: string }[]>([
    { id: 1, name: "HOT (ร้อน)" },
    { id: 2, name: "ICED (เย็น)" },
    { id: 3, name: "BLENDED (ปั่น)" },
  ]);

  const sweet = ref<{ id: number; name: string }[]>([
    { id: 4, name: "Normal 100%" },
    { id: 5, name: "Less Sweet 50%" },
    { id: 6, name: "Less Less Sweet 25%" },
    { id: 7, name: "Not Sweet 0%" },
  ]);

  const size = ref<{ id: number; name: string }[]>([
    { id: 8, name: "Size S (8 oz.)" },
    { id: 9, name: "Size M (12 oz.)" },
    { id: 10, name: "Size L (16 oz.)" },
  ]);

  const topping = ref<{ id: number; name: string }[]>([
    { id: 1, name: "Pearl (ไข่มุก)" },
    { id: 2, name: "Lava Pearl (ไข่มุกลาวา)" },
    { id: 3, name: "Fruity Jelly (เยลลี่ฟรุ๊ตสลัด)" },
    { id: 4, name: "Grass Jelly (เฉาก๊วย)" },
    { id: 5, name: "Brown Sugar Pearl (ไข่มุกบราวน์ชูการ์)" },
    { id: 6, name: "Milk Pudding (พุดดิ้งนม)" },
    { id: 7, name: "Whipped Cream (วิปครีม) " },
    { id: 8, name: "Whip Cheese (วิปชีส)" },
  ]);

  const addSweet = (item: { id: number; name: string }) => {
    if (sweetList.value.length > 0) {
      sweetList.value.splice(0);
      sweetList.value.push(item);
    } else {
      sweetList.value.push(item);
      addOndrink.value.push(item);
    }
  };

  const addSize = (item: { id: number; name: string }) => {
    if (sizeList.value.length > 0) {
      sizeList.value.splice(0);
      sizeList.value.push(item);
    } else {
      sizeList.value.push(item);
      addOndrink.value.push(item);
    }
  };
  const addType = (item: { id: number; name: string }) => {
    if (typeList.value.length > 0) {
      typeList.value.splice(0);
      typeList.value.push(item);
    } else {
      typeList.value.push(item);
      addOndrink.value.push(item);
    }
  };

  const addTopping = (item: { id: number; name: string }) => {
    toppingList.value.push(item);
  };

  function clear() {
    sweetList.value = [];
    sizeList.value = [];
    typeList.value = [];
    toppingList.value = [];
  }
  function deletetop(index: number) {
    toppingList.value.splice(index, 1);
  }
  function deletetype(index: number) {
    typeList.value.splice(index, 1);
  }
  function deletesize(index: number) {
    sizeList.value.splice(index, 1);
  }
  function deletesweet(index: number) {
    sweetList.value.splice(index, 1);
  }

  return {
    type,
    sweet,
    size,
    addSweet,
    sweetList,
    clear,
    addSize,
    addType,
    sizeList,
    typeList,
    topping,
    toppingList,
    addTopping,
    deletetop,
    deletesize,
    deletesweet,
    deletetype,
    addOndrink,
  };
});
