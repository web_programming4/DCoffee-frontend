import { ref } from "vue";
import { defineStore } from "pinia";
import orderitemService from "@/services/order-item";
import type OrderItem from "@/types/OrderItem";
// import { useLoadingStore } from "./loading";
// import { useMessageStore } from "./message";

export const useOrderItemStore = defineStore("orderItem", () => {
  // const loadingStore = useLoadingStore();
  // const messageStore = useMessageStore();
  // const successStore = useSuccessStore();
  const dialog = ref(false);
  const dialog1 = ref(false);
  const isTable = ref(true);
  const category = ref(1);
  const editedOrderItem = ref<OrderItem>({
    name: "",
    discount: 0,
    price: 0,
    amount: 0,
    total: 0,
  });
  const orderItem = ref<OrderItem[]>([]);
  async function getOrderItems() {
    try {
      const res = await orderitemService.getOrderItems();
      orderItem.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function deleteOrderItem(id: number) {
    try {
      const res = await orderitemService.deleteOrderItem(id);
      await getOrderItems();
    } catch (e) {
      console.log(e);
    }
  }

  async function saveOrderItem() {
    try {
      if (editedOrderItem.value.id) {
        const res = await orderitemService.updateOrderItem(
          editedOrderItem.value.id,
          editedOrderItem.value
        );
      } else {
        const res = await orderitemService.saveOrderItem(editedOrderItem.value);
      }

      dialog.value = false;
      clearOrderItem();
      await getOrderItems();
    } catch (e) {
      console.log(e);
    }
  }

  const editOrderItem = (orderItems: OrderItem) => {
    editedOrderItem.value = { ...orderItems };
    dialog.value = true;
  };

  function clearOrderItem() {
    editedOrderItem.value = {
      name: "",
      discount: 0,
      price: 0,
      amount: 0,
      total: 0,
    };
  }

  return {
    deleteOrderItem,
    saveOrderItem,
    dialog,
    dialog1,
    editOrderItem,
    clearOrderItem,
    category,
    orderItem,
    getOrderItems,
    isTable,
  };
});
