// import { computed, ref } from "vue";
// import { defineStore } from "pinia";
// import type Order from "@/types/Order";
// import OrderService from "@/services/order";
// import type Product from "@/types/Product";
// import { useAuthStore } from "./auth";
// import { useChipStore } from "./chip";
// import { useOrderItemStore } from "./orderItem";
// import { useMessageStore } from "./message";
// import product from "@/services/product";

// export const useOrderStore = defineStore("Order", () => {
//   const dialog = ref(false);
//   const dialogChips = ref(false);
//   const dialog1 = ref(false);
//   const isTable = ref(true);
//   const chipStore = useChipStore();
//   const cash = ref(0);
//   const drinkname = ref("");
//   const AddonName = ref("");
//   const AllToppingName = ref("");
//   const AllToppingPrice = ref(0);
//   const promotion = ref("");
//   const newsum = ref(0);
//   const messageStore = useMessageStore();
//   const orderItemStore = useOrderItemStore();
//   const authStore = useAuthStore();
//   const user = ref();

//   const editedOrders = ref<Order>({});
//   const drinkItem = ref<
//     {
//       id: number;
//       name: string;
//       type: string;
//       size: string;
//       price: number;
//       image: string;
//       categoryId: number;
//       amount: number;
//       sum: number;
//     }[]
//   >([]);

//   const orderList = ref<
//     {
//       id: number;
//       name: string;
//       discount: number;
//       price: number;
//       amount: number;
//       sum: number;
//       type: string;
//       size: string;
//       image: string;
//       categoryId: number;
//     }[]
//   >([]);
//   // const orderList2 = ref<
//   //   {
//   //     id: number;
//   //     name: string;
//   //     discount: number;
//   //     price: number;
//   //     amount: number;
//   //     total: number;
//   //   }[]
//   // >([]);

//   function clearOrder() {
//     orderList.value = [];

//     promotion.value = "";
//     newsum.value = 0;
//     cash.value = 0;
//   }

//   function orderder() {
//     for (let i = 0; i < orderList.value.length; i++) {
//       orderItemStore.orderItem.push({
//         id: orderList.value[i].id,
//         name: orderList.value[i].name,
//         discount: 0,
//         price: orderList.value[i].price,
//         amount: orderList.value[i].amount,
//         total: orderList.value[i].sum,
//       });
//     }
//     orderList.value = [];

//     promotion.value = "";
//     newsum.value = 0;
//     cash.value = 0;

//     console.log(orderItemStore.orderItem);
//   }

//   function addProduct(item: Product) {
//     if (item.categoryId === 1) {
//       dialogChips.value = true;
//       drinkname.value = item.name;
//       drinkItem.value.push({
//         id: item.id,
//         name: item.name,
//         type: item.type,
//         size: item.size,
//         price: item.price,
//         image: "",
//         categoryId: item.categoryId,
//         amount: 1,
//         sum: 1 * item.price,
//       });
//     } else {
//       for (let i = 0; i < orderList.value.length; i++) {
//         if (orderList.value[i].id === item.id) {
//           orderList.value[i].amount++;
//           orderList.value[i].sum = orderList.value[i].amount * item.price;
//           return;
//         }
//       }
//       orderList.value.push({
//         id: item.id,
//         name: item.name,
//         type: item.type,
//         size: item.size,
//         price: item.price,
//         discount: 0,
//         image: "",
//         categoryId: item.categoryId,
//         amount: 1,
//         sum: 1 * item.price,
//       });
//     }
//   }

//   function deleteProduct(index: number) {
//     orderList.value.splice(index, 1);
//   }
//   const sumPrice = computed(() => {
//     let sum = 0;
//     for (let i = 0; i < orderList.value.length; i++) {
//       sum = sum + orderList.value[i].sum;
//       newsum.value = sum;
//     }
//     if (sum >= 300) {
//       promotion.value = "ซื้อครบ 300 บาท ลดทันที 10 บาท";
//       newsum.value = sum - 10;
//     } else {
//       if (sum >= 150) {
//         promotion.value = "ซื้อครบ 150 บาท ลดทันที 5 บาท";
//         newsum.value = sum - 5;
//       }
//     }
//     return sum;
//   });

//   const sumAmount = computed(() => {
//     let sum = 0;
//     for (let i = 0; i < orderList.value.length; i++) {
//       sum = sum + orderList.value[i].amount;
//     }
//     return sum;
//   });
//   const orders = ref<Order[]>([]);
//   async function getOrders() {
//     try {
//       const res = await OrderService.getOrders();
//       orders.value = res.data;
//     } catch (e) {
//       console.log(e);
//     }
//   }

//   async function deleteOrder(id: number) {
//     try {
//       const res = await OrderService.deleteOrder(id);
//       await getOrders();
//     } catch (e) {
//       console.log(e);
//     }
//   }

//   async function openOrder() {
//     const user: { id: number } = authStore.getUser();
//     const orderItems = orderList.value.map(
//       (item) =>
//         <{ productId: number; amount: number }>{
//           productId: item.id,
//           amount: item.amount,
//         }
//     );

//     const order = { userId: user.id, orderItems: orderItems };
//     console.log(order);
//     try {
//       const res = await OrderService.saveOrder(order);
//       dialog.value = false;
//       clearOrder();
//       // await getOrders();
//     } catch (e) {
//       console.log(e);
//     }
//   }

//   // async function openOrder() {
//   //   const user: { id: number } = authStore.getUser();
//   //   const orderItmes = orderList.value.map(
//   //     (item) =>
//   //       <
//   //         {
//   //           id: number;
//   //           name: string;
//   //           discount: number;
//   //           price: number;
//   //           amount: number;
//   //           total: number;
//   //         }
//   //       >{
//   //         id: item.id,
//   //         name: item.name,
//   //         discount: item.discount,
//   //         price: item.price,
//   //         amount: item.amount,
//   //         total: item.sum,
//   //       }
//   //   );
//   //   const order = { userId: user, orderItems: orderItmes };
//   //   try {
//   //     const res = await OrderService.saveOrder(order);
//   //     dialog.value = false;
//   //     clearOrder();
//   //     await getOrders();
//   //   } catch (e) {
//   //     console.log(e);
//   //   }
//   // }

//   const savecash = (money: number) => {
//     if (money < sumPrice.value) {
//       messageStore.showError("ยอดเงินไม่พอ");
//     } else {
//       cash.value = money;
//       dialog.value = false;
//     }
//   };

//   const change = computed(function () {
//     if (cash.value > 0) {
//       if (newsum.value == 0) {
//         return orderList.value.reduce(() => cash.value - sumPrice.value, 0);
//       } else {
//         return orderList.value.reduce(() => cash.value - newsum.value, 0);
//       }
//     } else {
//       return orderList.value.reduce(() => cash.value - 0, 0);
//     }
//   });

//   const close = () => {
//     if (newsum.value == 0) {
//       cash.value = sumPrice.value;
//     } else {
//       cash.value = newsum.value;
//     }

//     dialog1.value = false;
//   };

//   const sweetList = ref<{ id: number; name: string; price: number }[]>([]);
//   const sizeList = ref<{ id: number; name: string; price: number }[]>([]);
//   const typeList = ref<{ id: number; name: string; price: number }[]>([]);
//   const toppingList = ref<{ id: number; name: string; price: number }[]>([]);
//   const type = ref<{ id: number; name: string; price: number }[]>([
//     { id: 1, name: "HOT (ร้อน)", price: 0 },
//     { id: 2, name: "ICED (เย็น) +10.-", price: 10 },
//     { id: 3, name: "BLENDED (ปั่น) +15.-", price: 15 },
//   ]);

//   const sweet = ref<{ id: number; name: string; price: number }[]>([
//     { id: 1, name: "Normal 100%", price: 0 },
//     { id: 2, name: "Less Sweet 50%", price: 0 },
//     { id: 3, name: "Less Less Sweet 25%", price: 0 },
//     { id: 4, name: "Not Sweet 0%", price: 0 },
//   ]);

//   const size = ref<{ id: number; name: string; price: number }[]>([
//     { id: 1, name: "Size S (8 oz.)", price: 0 },
//     { id: 2, name: "Size M (12 oz.) +5.-", price: 5 },
//     { id: 3, name: "Size L (16 oz.) +10.-", price: 10 },
//   ]);

//   const topping = ref<{ id: number; name: string; price: number }[]>([
//     { id: 1, name: "No Topping", price: 0 },
//     { id: 2, name: "Pearl (ไข่มุก) +5.-", price: 5 },
//     { id: 3, name: "Lava Pearl (ไข่มุกลาวา) +10.-", price: 10 },
//     { id: 4, name: "Fruity Jelly (เยลลี่ฟรุ๊ตสลัด) +10.-", price: 10 },
//     { id: 5, name: "Grass Jelly (เฉาก๊วย) +10.-", price: 10 },
//     { id: 6, name: "Brown Sugar Pearl (ไข่มุกบราวน์ชูการ์) +15.-", price: 15 },
//     { id: 7, name: "Milk Pudding (พุดดิ้งนม) +15.-", price: 15 },
//     { id: 8, name: "Whipped Cream (วิปครีม) +20.-", price: 20 },
//     { id: 9, name: "Whip Cheese (วิปชีส) +25.-", price: 25 },
//   ]);
//   const addSweet = (item: { id: number; name: string; price: number }) => {
//     if (sweetList.value.length > 0) {
//       sweetList.value.splice(0);
//       sweetList.value.push(item);
//     } else {
//       sweetList.value.push(item);
//     }
//   };
//   const addType = (item: { id: number; name: string; price: number }) => {
//     if (typeList.value.length > 0) {
//       typeList.value.splice(0);
//       typeList.value.push(item);
//     } else {
//       typeList.value.push(item);
//     }
//   };
//   const addSize = (item: { id: number; name: string; price: number }) => {
//     if (sizeList.value.length > 0) {
//       sizeList.value.splice(0);
//       sizeList.value.push(item);
//     } else {
//       sizeList.value.push(item);
//     }
//   };
//   const addTopping = (item: { id: number; name: string; price: number }) => {
//     for (let i = 0; i < toppingList.value.length; i++) {
//       if (toppingList.value[i].id === item.id) {
//         return;
//       }
//     }
//     toppingList.value.push(item);
//   };

//   const saveDrink = () => {
//     for (let i = 0; i < toppingList.value.length; i++) {
//       AllToppingName.value = AllToppingName.value + toppingList.value[i].name;
//       AllToppingPrice.value =
//         AllToppingPrice.value + toppingList.value[i].price;
//     }
//     AddonName.value =
//       " (" +
//       typeList.value[0].name +
//       " , " +
//       sweetList.value[0].name +
//       " , " +
//       sizeList.value[0].name +
//       " , " +
//       AllToppingName.value +
//       " )";

//     for (let i = 0; i < orderList.value.length; i++) {
//       if (orderList.value[i].id === drinkItem.value[0].id) {
//         if (
//           orderList.value[i].name ===
//           drinkItem.value[0].name + AddonName.value
//         ) {
//           orderList.value[i].amount++;
//           orderList.value[i].sum =
//             orderList.value[i].amount *
//             (drinkItem.value[0].price +
//               AllToppingPrice.value +
//               sizeList.value[0].price +
//               typeList.value[0].price);

//           dialogChips.value = false;
//           drinkItem.value = [];
//           chipStore.clear();
//           sweetList.value = [];
//           sizeList.value = [];
//           typeList.value = [];
//           toppingList.value = [];
//           return;
//         }
//       }
//     }
//     orderList.value.push({
//       id: drinkItem.value[0].id,
//       name: drinkItem.value[0].name + AddonName.value,
//       type: drinkItem.value[0].type,
//       size: drinkItem.value[0].size,
//       price:
//         drinkItem.value[0].price +
//         AllToppingPrice.value +
//         sizeList.value[0].price +
//         typeList.value[0].price,
//       discount: 0,
//       image: drinkItem.value[0].image,
//       categoryId: drinkItem.value[0].categoryId,
//       amount: 1,
//       sum:
//         1 * drinkItem.value[0].price +
//         AllToppingPrice.value +
//         sizeList.value[0].price +
//         typeList.value[0].price,
//     });

//     drinkItem.value = [];
//     dialogChips.value = false;
//     chipStore.clear();
//     sweetList.value = [];
//     sizeList.value = [];
//     typeList.value = [];
//     toppingList.value = [];
//     AllToppingName.value = "";
//     AllToppingPrice.value = 0;
//   };

//   const editOrder = (Order: Order) => {
//     editedOrders.value = { ...Order };
//     dialog.value = true;
//   };

//   function deletetop(index: number) {
//     toppingList.value.splice(index, 1);
//   }
//   function deletetype(index: number) {
//     typeList.value.splice(index, 1);
//   }
//   function deletesize(index: number) {
//     sizeList.value.splice(index, 1);
//   }
//   function deletesweet(index: number) {
//     sweetList.value.splice(index, 1);
//   }
//   function clear() {
//     sweetList.value = [];
//     sizeList.value = [];
//     typeList.value = [];
//     toppingList.value = [];
//   }

//   const inc = (item: {
//     id: number;
//     name: string;
//     type: string;
//     size: string;
//     price: number;
//     image: string;
//     categoryId: number;
//     amount: number;
//     sum: number;
//   }) => {
//     item.amount += 1;
//     item.sum = item.price * item.amount;
//     if (item.sum >= 150) {
//       promotion.value = "ซื้อครบ 150 บาท ลดทันที 5 บาท";
//       newsum.value = item.sum - 5;
//     }
//   };

//   const dec = (item: {
//     id: number;
//     name: string;
//     type: string;
//     size: string;
//     price: number;
//     image: string;
//     categoryId: number;
//     amount: number;
//     sum: number;
//   }) => {
//     if (item.amount < 2) {
//       return;
//     } else {
//       item.amount -= 1;
//       item.sum = item.price * item.amount;
//       if (item.sum < 150) {
//         promotion.value = "";
//         newsum.value = item.sum;
//       }
//     }
//   };

//   return {
//     orders,
//     inc,
//     promotion,
//     newsum,
//     dec,
//     clear,
//     close,
//     savecash,
//     cash,
//     change,
//     deletetop,
//     deletesize,
//     deletetype,
//     deletesweet,
//     deleteOrder,
//     dialog,
//     editedOrders,
//     clearOrder,
//     editOrder,
//     isTable,
//     dialog1,
//     getOrders,
//     addProduct,
//     deleteProduct,
//     sumAmount,
//     sumPrice,
//     orderList,
//     openOrder,
//     drinkItem,
//     dialogChips,
//     saveDrink,
//     sweet,
//     sweetList,
//     addSweet,
//     type,
//     size,
//     topping,
//     addSize,
//     addType,
//     addTopping,
//     sizeList,
//     typeList,
//     toppingList,
//     drinkname,
//     orderder,
//   };
// });
