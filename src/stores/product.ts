import { ref, watch } from "vue";
import { defineStore } from "pinia";
import productService from "@/services/product";
import type Product from "@/types/Product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  // const successStore = useSuccessStore();
  const dialog = ref(false);
  const products = ref<Product[]>([]);
  const category = ref(1);
  const editedProduct = ref<Product & { files: File[] }>({
    id: 0,
    name: "",
    type: "",
    size: "",
    price: 0,
    image: "No_Image_Available.jpg",
    files: [],
    categoryId: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedProduct.value = {
        id: 0,
        name: "",
        type: "",
        size: "",
        price: 0,
        image: "No_Image_Available.jpg",
        files: [],
        categoryId: 0,
      };
    }
  });

  watch(category, async (newCategory, oldCategory) => {
    await getProductsByCategory(newCategory);
  });

  async function getProductsByCategory(category: number) {
    // loadingStore.isLoading = true;
    try {
      console.log(category);
      const res = await productService.getProductsByCategory(category);
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getProducts() {
    // loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts({});
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Product ได้");
    }
  }

  async function getViewProducts() {
    // loadingStore.isLoading = true;
    try {
      const res = await productService.getViewProducts({});
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Product ได้");
    }
  }

  async function getViewProducts2() {
    // loadingStore.isLoading = true;
    try {
      const res = await productService.getViewProducts2({});
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Product ได้");
    }
  }

  async function getViewProducts3() {
    // loadingStore.isLoading = true;
    try {
      const res = await productService.getViewProducts3({});
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Product ได้");
    }
  }

  async function deleteProduct(id: number) {
    // loadingStore.isLoading = true;
    try {
      const res = await productService.deleteProduct(id);
      await getProducts();
      // successStore.showsuccess("ลบสำเร็จ")
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Product ได้");
    }
  }

  async function saveProduct() {
    // loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }

      dialog.value = false;
      await getProducts();
      // successStore.showsuccess("บันทึกสำเร็จ")
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Product ได้");
    }
  }

  const editProduct = (product: Product) => {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  };

  // const editProduct = (product: Product) => {
  //   editedProduct.value = { ...product };
  //   dialog.value = true;
  // };

  function clearProduct() {
    editedProduct.value = {
      id: 0,
      name: "",
      type: "",
      size: "",
      price: 0,
      categoryId: 1,
      image: "No_Image_Available.jpg",
      files: [],
    };
  }

  return {
    products,
    deleteProduct,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    getProducts,
    getProductsByCategory,
    category,
    clearProduct,
    getViewProducts,
    getViewProducts2,
    getViewProducts3,
  };
});
