import { ref } from "vue";
import { defineStore } from "pinia";
import check_matService from "@/services/check_mat";
import cmdService from "@/services/cmd";
import type Check_mat from "@/types/check_mat";
import type Cmd from "@/types/cmd";

export const useCheck_matStore = defineStore("check_mat", () => {
  const dialog = ref(false);
  const editedCheck_mat = ref<Cmd>({
    cmd_name: "",
    cmd_qty_last: 0,
    cmd_qty_remain: 0,
    cmd_qty_expire: 0,
  });
  const cmd = ref<Cmd[]>([]);
  const check_mat = ref<Check_mat[]>([]);

  async function getCheckMat() {
    try {
      const res = await cmdService.getCmds();
      cmd.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }

  async function saveCheck_mat() {
    try {
      if (editedCheck_mat.value.id) {
        const res = await cmdService.updateCmd(
          editedCheck_mat.value.id,
          editedCheck_mat.value
        );
      } else {
        const res = await cmdService.saveCmd(editedCheck_mat.value);
      }

      dialog.value = false;
      clearCheck_mat();
      await getCheckMat();
    } catch (e) {
      console.log(e);
    }
  }
  function clearCheck_mat() {
    editedCheck_mat.value = {
      cmd_name: "",
      cmd_qty_last: 0,
      cmd_qty_remain: 0,
      cmd_qty_expire: 0,
    };
  }

  const editCheck_mat = (check_mat: Check_mat) => {
    editedCheck_mat.value = JSON.parse(JSON.stringify(check_mat));
    dialog.value = true;
  };

  return {
    dialog,
    getCheckMat,
    cmd,
  };
});
