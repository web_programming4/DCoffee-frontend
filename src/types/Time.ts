export default interface Time {
  cio_id?: number;
  cio_date: string;
  cio_time_in: string;
  cio_time_out: string;
  cio_total_hour: number;
  employeeId: number;
  salaryId: number;

  createAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
