export default interface Employees {
  id?: number;
  name: string;
  house_no: string;
  village_no: string;
  sub_district: string;
  district: string;
  province: string;
  email: string;
  tel: string;
  position: string;
  hourly_wage: number;
  userId: number;
  image: string;
  checkTime?: number;

  createAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
