export default interface User {
  id?: number;
  name: string;
  login: string;
  password: string;

  createAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
