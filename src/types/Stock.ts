export default interface Stock {
  id?: number;
  name: string;
  min_qty: number;
  qty: number;
  unit: string;
  price_per_unit: number;

  createAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
