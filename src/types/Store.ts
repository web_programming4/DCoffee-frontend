export default interface Store {
  id?: number;
  name: string;
  address: string;
  tel: string;

  createAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
