export default interface Check_mat {
  id?: number;
  check_mat_date: Date;
  check_mat_time: string;
  emp_id?: number;

  createAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
