export default interface Customer {
  id?: number;
  name: string;
  tel: number;
  point: string;

  createAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
