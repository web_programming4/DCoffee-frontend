import type Order from "./Order";
import type Product from "./Product";

export default interface OrderItem {
  id?: number;
  name?: string;
  discount?: number;
  price?: number;
  amount?: number;
  total?: number;
  product?: Product[];
  order?: Order;
  createDate?: Date;
  updateDate?: Date;
  deleteDate?: Date;
}
