export default interface Salary {
  id?: number;
  datess: number;
  workhour: number;
  salary: number;

  createAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
