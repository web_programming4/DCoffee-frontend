import type Category from "./Category";

export default interface Product {
  id: number;
  name: string;
  type: string;
  size: string;
  price: number;
  image: string;
  categoryId: number;
  category?: Category;

  createAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
