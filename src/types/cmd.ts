export default interface Cmd {
  id?: number;
  cmd_name: string;
  cmd_qty_last: number;
  cmd_qty_remain: number;
  mat_id?: number;
  check_mat_id?: number;
  cmd_qty_expire: number;

  createAt?: Date;
  updateAt?: Date;
  deleteAt?: Date;
}
