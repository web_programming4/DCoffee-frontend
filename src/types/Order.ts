import type Customer from "./Customer";
import type Employees from "./Empolyees";
import type OrderItem from "./OrderItem";
import type Store from "./Store";
import type User from "./User";

export default interface Order {
  id?: number;
  // name?: string;
  queue?: number;
  date?: string;
  time?: string;
  // discount?: number;
  amount?: number;
  total?: number;
  received?: number;
  change?: number;
  payment?: string;
  orderItemId?: OrderItem[];
  // employee?: Employees;
  userId?: User;
  // customer?: Customer;
  stores?: Store;
  createDate?: Date;
  updateDate?: Date;
  deleteDate?: Date;
}
