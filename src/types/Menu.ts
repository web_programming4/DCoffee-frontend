export default interface Drink {
  id: number;
  name: string;
  price: number;
  img: string;
  qty: number;
}

export default interface Sweet {
  id: number;
  name: string;
  price: number;
  img: string;
  qty: number;
}

export default interface Food {
  id: number;
  name: string;
  price: number;
  img: string;
  qty: number;
}
