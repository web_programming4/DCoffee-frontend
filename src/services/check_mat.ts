import type Stock from "@/types/check_mat";
import http from "./axios";

function getCheck_mat() {
  return http.get("/materials");
}

function saveCheck_mat(stock: Stock) {
  return http.post("/materials", stock);
}

function updateCheck_mat(id: number, stock: Stock) {
  return http.patch(`/materials/${id}`, stock);
}

function deleteCheck_mat(id: number) {
  return http.delete(`/materials/${id}`);
}

export default {
  getCheck_mat,
  saveCheck_mat,
  updateCheck_mat,
  deleteCheck_mat,
};
