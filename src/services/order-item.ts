import http from "./axios";
import type OrderItem from "@/types/OrderItem";

function getOrderItems() {
  return http.get("/order-item");
}

function saveOrderItem(orderItem: OrderItem) {
  return http.post("/order-item", orderItem);
}

// function saveOrderItem(orderItem: {
//   orderItems: {
//     productId: number;
//     name: string;
//     discount: number;
//     price: number;
//     amount: number;
//     total: number;
//   }[];
//   userId: number;
// }) {
//   return http.post("/order-item", orderItem);
// }

function updateOrderItem(id: number, orderItem: OrderItem) {
  return http.patch(`/order-item/${id}`, orderItem);
}

function deleteOrderItem(id: number) {
  return http.delete(`/order-item/${id}`);
}

export default {
  getOrderItems,
  saveOrderItem,
  updateOrderItem,
  deleteOrderItem,
};
