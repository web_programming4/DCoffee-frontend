import type Salary from "@/types/Salary";
import http from "./axios";

function getSalarys() {
  return http.get("/salarys");
}

function saveSalary(salary: Salary) {
  return http.post("/salarys", salary);
}

function updateSalary(id: number, salary: Salary) {
  return http.patch(`/salarys/${id}`, salary);
}

function deleteSalary(id: number) {
  return http.delete(`/salarys/${id}`);
}

export default { getSalarys, saveSalary, updateSalary, deleteSalary };
