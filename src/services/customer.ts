import type Customer from "@/types/Customer";
import http from "./axios";

function getCustomers() {
  return http.get("/customer");
}

function getViewCustomers() {
  return http.get("/reports/customer");
}

function saveCustomer(customer: Customer) {
  return http.post("/customer", customer);
}

function updateCustomer(id: number, customer: Customer) {
  return http.patch(`/customer/${id}`, customer);
}

function deleteCustomer(id: number) {
  return http.delete(`/customer/${id}`);
}

export default {
  getCustomers,
  saveCustomer,
  updateCustomer,
  deleteCustomer,
  getViewCustomers,
};
