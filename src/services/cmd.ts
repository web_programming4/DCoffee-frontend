import type Cmd from "@/types/cmd";
import http from "./axios";

function getCmds() {
  return http.get("/check-material-detail");
}

function saveCmd(stock: Cmd) {
  return http.post("/check-material-detail", stock);
}

function updateCmd(id: number, stock: Cmd) {
  return http.patch(`/check-material-detail/${id}`, stock);
}

function deleteCmd(id: number) {
  return http.delete(`/check-material-detail/${id}`);
}

export default { getCmds, saveCmd, updateCmd, deleteCmd };
