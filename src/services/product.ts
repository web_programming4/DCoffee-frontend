import type Product from "@/types/Product";
import http from "./axios";

function getProductsByCategory(category: number) {
  return http.get(`/products/category/${category}`);
}

function getProducts(params: any) {
  return http.get("/products", { params: params });
}

function getViewProducts(params: any) {
  return http.get("/reports2/product", { params: params });
}

function getViewProducts2(params: any) {
  return http.get("/reports3/product", { params: params });
}

function getViewProducts3(params: any) {
  return http.get("/reports4/product", { params: params });
}

function saveProduct(product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("type", product.type);
  formData.append("size", product.size);
  formData.append("price", `${product.price}`);
  formData.append("file", product.files[0]);
  formData.append("categoryId", `${product.categoryId}`);
  return http.post("/products", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function updateProduct(id: number, product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("typr", product.type);
  formData.append("size", product.size);
  formData.append("price", `${product.price}`);
  formData.append("categoryId", `${product.categoryId}`);
  console.log(product.files);
  if (product.files) {
    formData.append("files", product.files[0]);
  }
  return http.patch(`/products/${id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function deleteProduct(id: number) {
  return http.delete(`/products/${id}`);
}

export default {
  getProducts,
  saveProduct,
  updateProduct,
  deleteProduct,
  getProductsByCategory,
  getViewProducts,
  getViewProducts2,
  getViewProducts3,
};
