import type Employees from "@/types/Empolyees";
import http from "./axios";

function getEmployees(params: any) {
  return http.get(`/employees`, { params: params });
}

function getViewEmployees(params: any) {
  return http.get(`/reports2/employees`, { params: params });
}

function saveEmployee(employee: Employees & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", employee.name);
  formData.append("house_no", employee.house_no);
  formData.append("village_no", employee.village_no);
  formData.append("sub_district", employee.sub_district);
  formData.append("district", employee.district);
  formData.append("province", employee.province);
  formData.append("email", employee.email);
  formData.append("tel", employee.tel);
  formData.append("position", employee.position);
  formData.append("hourly_wage", `${employee.hourly_wage}`);
  formData.append("userId", `${employee.userId}`);
  formData.append("file", employee.files[0]);

  return http.post("/employees", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

// function updateEmployee(id: number, employee: Employees) {
//   return http.patch(`/employees/${id}`, employee);
// }

function updateEmployee(id: number, employee: Employees & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", employee.name);
  formData.append("house_no", employee.house_no);
  formData.append("village_no", employee.village_no);
  formData.append("sub_district", employee.sub_district);
  formData.append("district", employee.district);
  formData.append("province", employee.province);
  formData.append("email", employee.email);
  formData.append("tel", employee.tel);
  formData.append("position", employee.position);
  formData.append("hourly_wage", `${employee.hourly_wage}`);
  formData.append("userId", `${employee.userId}`);
  console.log(employee.files);
  if (employee.files) {
    formData.append("files", employee.files[0]);
  }
  return http.patch(`/employees/${id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function deleteEmployee(id: number) {
  return http.delete(`/employees/${id}`);
}

export default {
  getEmployees,
  saveEmployee,
  updateEmployee,
  deleteEmployee,
  getViewEmployees,
};
