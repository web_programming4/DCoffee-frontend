import type Store from "@/types/Store";
import http from "./axios";

function getStores() {
  return http.get("/stores");
}

function saveStore(store: Store) {
  return http.post("/stores", store);
}

function updateStore(id: number, store: Store) {
  return http.patch(`/stores/${id}`, store);
}

function deleteStore(id: number) {
  return http.delete(`/stores/${id}`);
}

export default { getStores, saveStore, updateStore, deleteStore };
