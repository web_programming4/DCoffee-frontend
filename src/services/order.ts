import type Order from "@/types/Order";
import http from "./axios";

function getOrders() {
  return http.get("/orders");
}

function saveOrder(order: {
  orderItemId: {
    name: string;
    // discount: number;
    price: number;
    amount: number;
    total: number;
    productId: number;
  }[];
  userId: number;
}) {
  return http.post("/orders", order);
}

// function saveOrder(order: Order) {
//   return http.post("/orders", order);
// }

function updateOrder(id: number, order: Order) {
  return http.patch(`/orders/${id}`, order);
}

function deleteOrder(id: number) {
  return http.delete(`/orders/${id}`);
}

export default { getOrders, saveOrder, updateOrder, deleteOrder };
