import type Time from "@/types/Time";
import http from "./axios";

function getTimes() {
  return http.get("/check-time");
}

function saveTime(time: Time) {
  return http.post("/check-time", time);
}

function updateTime(id: number, time: Time) {
  return http.patch(`/check-time/${id}`, time);
}

function deleteTime(id: number) {
  return http.delete(`/check-time/${id}`);
}

export default { getTimes, saveTime, updateTime, deleteTime };
