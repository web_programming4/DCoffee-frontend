import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import LoginView from "@/views/LoginView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        default: HomeView,
        // menu: () => import("@/components/main/MainMenu.vue"),
        // header: () => import("@/components/header/HeaderHome.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/login",
      name: "login",
      components: {
        default: LoginView,
      },
      meta: {
        layout: "FullLayout",
      },
    },

    {
      path: "/user",
      name: "user",
      components: {
        default: () => import("../views/user/UserView.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderUser.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/stock",
      name: "stock",
      components: {
        default: () => import("../components/stock/StockView.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderStock.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/store",
      name: "store",
      components: {
        default: () => import("../components/store/StoreView.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderStore.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/employees",
      name: "employees",
      components: {
        default: () => import("../components/employee/EmployeesTest.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderEmployee.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/pos",
      name: "pos",
      components: {
        default: () => import("../views/pos/PosViewCopy.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderPOS.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/viewDrink",
      name: "viewDrink",
      components: {
        default: () => import("../components/ProductView/viewTable.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderProduct.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/viewSweet",
      name: "viewSweet",
      components: {
        default: () => import("../components/ProductView/viewFood.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderProduct.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/viewFood",
      name: "viewFood",
      components: {
        default: () => import("../components/ProductView/viewDrink.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderProduct.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/products-page",
      name: "products-page",
      components: {
        default: () => import("../views/ProductPaginationView.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderProduct.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/customer",
      name: "customer",
      components: {
        default: () => import("../components/customer/CustomersView.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderCustomer.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/viewcus",
      name: "viewcus",
      components: {
        default: () => import("../components/customer/CustomersView.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderCustomer.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/login",
      name: "login",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/LoginView.vue"),
    },
    {
      path: "/check_stock",
      name: "check_stock",
      components: {
        default: () => import("../components/check_stock/CheckStock.vue"),
        // menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderCheckStock.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/timework",
      name: "timework",
      components: {
        default: () => import("../components/timework/TimeView.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderTime.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/purchase_history",
      name: "purchase_history",
      components: {
        default: () =>
          import("../components/purchase_history/PurchaseView.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderOrder.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/order_item",
      name: "order_item",
      components: {
        default: () => import("../components/order_item/orderItemView.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderOrderItem.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/graph2",
      name: "graph2",
      components: {
        default: () => import("../components/graph/Graph2Show.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderCheckStock.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/graph1",
      name: "graph1",
      components: {
        default: () => import("../components/graph/GraphShow.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderCheckStock.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/graph",
      name: "graph",
      components: {
        default: () => import("../views/ShowGraph.vue"),
        menu: () => import("@/components/main/MainMenu.vue"),
        header: () => import("@/components/header/HeaderGraph.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
  ],
});

function isLogin() {
  const user = localStorage.getItem("user");
  if (user) {
    return true;
  }
  return false;
}

router.beforeEach((to) => {
  if (to.meta.requiresAuth && !isLogin()) {
    return {
      path: "/login",
      query: { redirect: to.fullPath },
    };
  }
});

export default router;
